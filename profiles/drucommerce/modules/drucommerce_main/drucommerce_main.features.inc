<?php
/**
 * @file
 * drucommerce_main.features.inc
 */

/**
 * Implements hook_default_commerce_discount().
 */
function drucommerce_main_default_commerce_discount() {
  $items = array();
  $items['discount_10_off'] = entity_import('commerce_discount', '{
    "name" : "discount_10_off",
    "label" : "10% off",
    "type" : "order_discount",
    "status" : "1",
    "component_title" : "10 percent",
    "sort_order" : "10",
    "commerce_discount_offer" : {
      "type" : "percentage",
      "commerce_percentage" : { "und" : [ { "value" : "10.00" } ] }
    },
    "inline_conditions" : { "und" : [
        {
          "condition_name" : "commerce_order_compare_order_amount",
          "condition_settings" : {
            "operator" : "\\u003E",
            "total" : { "amount" : 0, "currency_code" : "USD" },
            "condition_logic_operator" : null
          },
          "condition_negate" : 0
        }
      ]
    },
    "commerce_compatibility_strategy" : [],
    "commerce_compatibility_selection" : []
  }');
  return $items;
}

/**
 * Implements hook_commerce_product_default_types().
 */
function drucommerce_main_commerce_product_default_types() {
  $items = array(
    'product' => array(
      'type' => 'product',
      'name' => 'Product',
      'description' => 'A basic product type.',
      'help' => '',
      'revision' => 1,
    ),
  );
  return $items;
}

/**
 * Implements hook_commerce_tax_default_types().
 */
function drucommerce_main_commerce_tax_default_types() {
  $items = array(
    'sales_tax' => array(
      'name' => 'sales_tax',
      'display_title' => 'Sales tax',
      'description' => 'A basic type for taxes that do not display inclusive with product prices.',
      'display_inclusive' => 0,
      'round_mode' => 0,
      'rule' => 'commerce_tax_type_sales_tax',
      'module' => 'commerce_tax_ui',
      'title' => 'Sales tax',
      'admin_list' => TRUE,
    ),
    'vat' => array(
      'name' => 'vat',
      'display_title' => 'VAT',
      'description' => 'A basic type for taxes that display inclusive with product prices.',
      'display_inclusive' => 1,
      'round_mode' => 1,
      'rule' => 'commerce_tax_type_vat',
      'module' => 'commerce_tax_ui',
      'title' => 'VAT',
      'admin_list' => TRUE,
    ),
  );
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function drucommerce_main_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
  if ($module == "taxonomy_display" && $api == "taxonomy_display") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function drucommerce_main_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function drucommerce_main_image_default_styles() {
  $styles = array();

  // Exported image style: product_images_320x460.
  $styles['product_images_320x460'] = array(
    'label' => 'product_images_320x460',
    'effects' => array(
      12 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 320,
          'height' => 460,
        ),
        'weight' => -9,
      ),
    ),
  );

  // Exported image style: product_images__800x800_.
  $styles['product_images__800x800_'] = array(
    'label' => 'Product images (800x800)',
    'effects' => array(
      15 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 800,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => -10,
      ),
      14 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => '',
          'height' => 800,
          'upscale' => 0,
        ),
        'weight' => -9,
      ),
      18 => array(
        'name' => 'canvasactions_definecanvas',
        'data' => array(
          'RGB' => array(
            'HEX' => '#fff',
          ),
          'under' => 1,
          'exact' => array(
            'width' => 800,
            'height' => 800,
            'xpos' => 'center',
            'ypos' => 'center',
          ),
          'relative' => array(
            'leftdiff' => '',
            'rightdiff' => '',
            'topdiff' => '',
            'bottomdiff' => '',
          ),
        ),
        'weight' => 3,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function drucommerce_main_node_info() {
  $items = array(
    'product' => array(
      'name' => t('Product'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
