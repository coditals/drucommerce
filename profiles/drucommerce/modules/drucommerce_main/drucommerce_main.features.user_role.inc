<?php
/**
 * @file
 * drucommerce_main.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function drucommerce_main_user_default_roles() {
  $roles = array();

  // Exported role: Manager.
  $roles['Manager'] = array(
    'name' => 'Manager',
    'weight' => 2,
  );

  return $roles;
}
