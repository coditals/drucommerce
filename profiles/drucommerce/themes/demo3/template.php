<?php


function demo3_preprocess_node(&$vars) {
  $node_type_suggestion_key = array_search('node__' . $vars['type'], $vars['theme_hook_suggestions']);
  if ($node_type_suggestion_key !== FALSE) {
    array_splice($vars['theme_hook_suggestions'], $node_type_suggestion_key + 1, 0, array('node__' . $vars['type'] . '__' . $vars['view_mode']));
  }
}

function demo3_css_alter(&$css) {
  // Remove defaults.css file.
  unset($css[drupal_get_path('module', 'locale') . '/locale.css']);
  unset($css[drupal_get_path('module','system').'/system.menus.css']);  
}

function demo3_preprocess_html(&$vars) {
  $viewport = array(
   '#tag' => 'meta',
   '#attributes' => array(
     'name' => 'viewport',
     'content' => 'width=device-width, initial-scale=1, maximum-scale=1',
   ),
  );
  drupal_add_html_head($viewport, 'viewport');
}

function demo3_preprocess_page(&$variables) {
  if (!empty($variables['node']) && $variables['node']->type == 'product') {
    $variables['title'] = FALSE;
  }
}

