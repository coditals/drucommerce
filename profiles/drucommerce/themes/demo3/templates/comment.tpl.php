<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
	<div class="comment-autor">
		<span class="commenter-name">
		  <?php print $author; ?>
		</span>
		<span class="comment-permalink">
		  <?php print $permalink; ?>
		</span>
	</div>
	<div class="comment-body">
		<?php
		    // We hide the comments and links now so that we can render them later.
		    hide($content['links']);
		    print render($content);
		  ?>
		  <?php if ($signature): ?>
		  <div class="user-signature clearfix">
		    <?php print $signature; ?>
		  </div>
		  <?php endif; ?>
		</div> <!-- /.content -->
	</div>
	<div class="comment-info">
		<span class="comment-time">
		  <?php print $created; ?>
		</span>
		<?php print render($content['links']); ?>
	</div>
</div>
