<?php if ($quantity_raw && !$total ) { ?>
	<a href="/cart">
		<div class="line-item-summary">
		  	<div class="line-item-quantity">
		   		 <span class="line-item-quantity-label">Cart: </span><span class="line-item-quantity-raw"><?php print $quantity_raw; ?></span>
		 	 </div>
	 	 </div>
	</a>
<?php } else { ?>
	<div class="line-item-summary">
	  <?php if ($quantity_raw): ?>
	  <div class="line-item-quantity">
	    <span class="line-item-quantity-raw"><?php print $quantity_raw; ?></span> <span class="line-item-quantity-label"><?php print $quantity_label; ?></span>
	  </div>
	  <?php endif; ?>
	  <?php if ($total): ?>
	  <div class="line-item-total">
	    <span class="line-item-total-label"><?php print $total_label; ?></span> <span class="line-item-total-raw"><?php print $total; ?></span>
	  </div>
	  <?php endif; ?>
	  <?php print $links; ?>
	</div>
<?php } ?>