<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  

   <div class="content clearfix"<?php print $content_attributes; ?>>
  		<div class="row">
  			<div class="product-info col-md-4">
  				<div class="product-title">
  					<h2<?php print render($title_prefix); ?>
  					  <?php print $title_attributes; ?>>
  					    <span class="product-name"><?php print $title; ?></span>
  					  
  						<?php print render($title_suffix); ?>
  						<span class="by">by</span> <?php print render($content['field_brand']); ?>
  					</h2>
  				</div>
  				
  				<?php
  				  // We hide the comments and links now so that we can render them later.
  				  hide($content['comments']);
  				  hide($content['links']);
  				  hide($content['field_image']);
  				  hide($content['field_images']);
  				  print render($content);
  				?>
  			</div>
  			<div class="product-images col-md-8">
  				<?php print render($content['field_image']); ?>
  				<?php print render($content['field_images']); ?>
  			</div>
  		</div>
  
  
      </div>
</div>
