<header>
		<div class="page_top">
			<div class="container-fluid">
			 	<?php print render($page['page_top_left']); ?>
			 	<?php print render($page['page_top_right']); ?>
			 </div>
		 </div>
	
	<div class="container-fluid">
		 <div class="row header-main">
		 	<div class="col-md-8 logo">
		 		<?php if ($logo): ?>
		 		       <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
		 		         <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
		 		       </a>
		 		     <?php endif; ?>
		 	</div>
		 	<div class="col-md-4 header_right">
		 		<div class="header-block">
		 	 		<?php print render($page['header_right']); ?>
		 	 	</div>
		 	</div>
		 </div>
	 </div>
	 <div class="main_menu">
		 <div class="container-fluid">
		 	<?php print render($page['header_main_menu']); ?>		 
		 </div>
	</div>
	<?php if ($breadcrumb): ?>
	     <div id="breadcrumb">
	     	<div class="container-fluid">
	     		<?php // print $breadcrumb; ?>
	     	</div>
	     </div>
	<?php endif; ?>
</header>
<div class="container-fluid main-container">
	<?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
	<div class="row">
		<?php if ($page['left']): ?>
			<aside class="col-md-3 col-lg-2 left">
				<?php print render($page['left']); ?>
			</aside>
		<?php endif; ?>
	<div class="<?php 
	if ($page['left'] && $page['right']) print 'col-md-6 col-lg-7';
	elseif ($page['right']) print 'col-md-9';
	elseif ($page['left']) print 'col-md-9  col-lg-10';
	else print 'col-md-12';
	?>">
		<?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
		       <?php print render($page['help']); ?>
		       <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
		<?php print render($page['content']); ?>
	</div>
	<?php if ($page['right']): ?>
		<aside class="col-md-3 right">
			<?php print render($page['right']); ?>
		</aside>
	<?php endif; ?>
	</div>
</div>
<footer>
	<div class="container-fluid">
		<div class="footer_firstcolumn col-md-3">
			<?php print render($page['footer_firstcolumn']); ?>
		</div>
		<div class="footer_seconfcolumn col-md-3">
			<?php print render($page['footer_seconfcolumn']); ?>
		</div>
		<div class="footer_thirdcolumn col-md-3">
			<?php print render($page['footer_thirdcolumn']); ?>
		</div>
		<div class="footer_fourthcolumn col-md-3">
			<?php print render($page['footer_fourthcolumn']); ?>
		</div>
		<div class="footer col-md-12">
			<?php print render($page['footer']); ?>
		</div>
	</div>
</footer>



