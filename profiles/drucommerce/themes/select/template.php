<?php


function select_preprocess_node(&$vars) {
  $node_type_suggestion_key = array_search('node__' . $vars['type'], $vars['theme_hook_suggestions']);
  if ($node_type_suggestion_key !== FALSE) {
    array_splice($vars['theme_hook_suggestions'], $node_type_suggestion_key + 1, 0, array('node__' . $vars['type'] . '__' . $vars['view_mode']));
  }
}

function select_css_alter(&$css) {
  // Remove defaults.css file.
  unset($css[drupal_get_path('module', 'locale') . '/locale.css']);
  unset($css[drupal_get_path('module','system').'/system.menus.css']);
}

function select_preprocess_html(&$vars) {
  $viewport = array(
   '#tag' => 'meta',
   '#attributes' => array(
     'name' => 'viewport',
     'content' => 'width=device-width, initial-scale=1, maximum-scale=1',
   ),
  );
  drupal_add_html_head($viewport, 'viewport');
}

function select_fieldset($variables) {
  $element = $variables ['element'];
  element_set_attributes($element, array('id'));
  _form_set_class($element, array('form-wrapper panel panel-smart'));
  
  $output = '<fieldset' . drupal_attributes($element ['#attributes']) . '>';
  if (!empty($element ['#title'])) {
  
  	if (arg(0) == 'checkout') {
  		// For chekout pages.
  		$output .= '<div class="panel-heading">
  										<h3 class="panel-title">
  											' . $element ['#title'] . '
  										</h3>
  									</div>';
  		
  	}
  	
  	else {
  	$output .= '<legend><span class="fieldset-legend">' . $element ['#title'] . '</span></legend>';
  	}
    
  }
  $output .= '<div class="fieldset-wrapper">';
  if (!empty($element ['#description'])) {
    $output .= '<div class="fieldset-description">' . $element ['#description'] . '</div>';
  }
  $output .= $element ['#children'];
  if (isset($element ['#value'])) {
    $output .= $element ['#value'];
  }
  $output .= '</div>';
  $output .= "</fieldset>\n";
  return $output;
}

function select_preprocess_comment(&$vars) {
  // Change the Permalink to display #1 instead of 'Permalink'
  $comment = $vars['comment'];
  $uri = entity_uri('comment', $comment);
  $uri['options'] += array('attributes' => array(
    'class' => 'permalink',
    'rel' => 'bookmark',
  ));
  $vars['permalink'] = l('#' . $vars['id'], $uri['path'], $uri['options']);
}