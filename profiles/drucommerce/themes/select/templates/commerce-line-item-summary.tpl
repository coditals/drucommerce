<div class="line-item-summary">
  <?php if ($quantity_raw): ?>
  <div class="line-item-quantity">
    Cart: <span class="line-item-quantity-raw"><?php print $quantity_raw; ?></span>
  </div>
  <?php endif; ?>
  <?php if ($total): ?>
  <div class="line-item-total">
    <span class="line-item-total-label"><?php print $total_label; ?></span> <span class="line-item-total-raw"><?php print $total; ?></span>
  </div>
  <?php endif; ?>
  <?php print $links; ?>
</div>